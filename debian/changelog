python-cmaes (0.11.1-1) unstable; urgency=medium

  * New upstream version.
  * Change build system to pyproject.

 -- Gard Spreemann <gspr@nonempty.org>  Sun, 13 Oct 2024 23:41:00 +0200

python-cmaes (0.10.0-2) unstable; urgency=medium

  * Don't try to run tests from files without any. (Closes: #1056463)

 -- Gard Spreemann <gspr@nonempty.org>  Thu, 14 Dec 2023 08:35:45 +0100

python-cmaes (0.10.0-1) unstable; urgency=medium

  * New upstream version.
  * Drop build and test depends on python3-exceptiongroup, they are once
    again correctly transitive dependencies.

 -- Gard Spreemann <gspr@nonempty.org>  Sun, 23 Jul 2023 12:37:18 +0200

python-cmaes (0.9.1-1) unstable; urgency=medium

  * New upstream version.

 -- Gard Spreemann <gspr@nonempty.org>  Sun, 08 Jan 2023 14:07:07 +0100

python-cmaes (0.9.0-4) unstable; urgency=medium

  * Build-depend and autopkgtest-depend on python3-exceptiongroup to fix
    testing with Python 3.10.

 -- Gard Spreemann <gspr@nonempty.org>  Tue, 03 Jan 2023 19:51:47 +0100

python-cmaes (0.9.0-3) unstable; urgency=medium

  * Set an even longer test deadline due to flakiness on certain
    architectures.

 -- Gard Spreemann <gspr@nonempty.org>  Tue, 03 Jan 2023 16:30:24 +0100

python-cmaes (0.9.0-2) unstable; urgency=medium

  * Add patch to set a long test deadline in order to avoid
    flakiness. (Closes: #1027466)
  * Standards-version 4.6.2, no changes needed.

 -- Gard Spreemann <gspr@nonempty.org>  Sun, 01 Jan 2023 11:48:11 +0100

python-cmaes (0.9.0-1) unstable; urgency=medium

  * New upstream version.
  * Suggest SciPy.
  * Add python3-hypothesis as build-dependency for tests.
  * Run autopkgtests both with and without SciPy, testing both the
    internally implemented functions, and the SciPy-based ones.

 -- Gard Spreemann <gspr@nonempty.org>  Thu, 10 Nov 2022 21:44:20 +0100

python-cmaes (0.8.2-4) unstable; urgency=medium

  * Drop erroneous nose dependency. (Closes: #1018471)

 -- Gard Spreemann <gspr@nonempty.org>  Tue, 30 Aug 2022 18:30:39 +0200

python-cmaes (0.8.2-3) unstable; urgency=medium

  * Autopkgtest depends on python3-cmaes.

 -- Gard Spreemann <gspr@nonempty.org>  Fri, 08 Jul 2022 23:46:01 +0200

python-cmaes (0.8.2-2) unstable; urgency=medium

  * Add watch file.
  * Add upstream metadata file.
  * Add autopkgtests.
  * Bump standards-version to 4.6.1, no changes needed.

 -- Gard Spreemann <gspr@nonempty.org>  Fri, 08 Jul 2022 15:21:00 +0200

python-cmaes (0.8.2-1) unstable; urgency=medium

  * Initial release (Closes: #1013337)

 -- Gard Spreemann <gspr@nonempty.org>  Wed, 01 Jun 2022 18:42:14 +0200
