#!/bin/bash

set -e
set -u

testdir=$PWD/tests
cp -R $testdir $AUTOPKGTEST_TMP
cd $AUTOPKGTEST_TMP

for py3ver in $(py3versions -vs)
do
    for file in tests/test_*.py
    do
        echo "Running test ${file} with Python ${py3ver}."
        /usr/bin/python${py3ver} -B -m unittest ${file}
    done
done
